package com.proyecto.proyectoSpringJettyMaven.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.proyecto.proyectoSpringJettyMaven.App;

@Controller
@SuppressWarnings("UnusedDeclaration")
public class IndexController {

    @Value("${example.message}")
    private String message;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    @ResponseBody
    public String showIndex() {
        return message;
    }
    
    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ResponseBody
    public String showIndex(@RequestBody App aaa) {
        return aaa.getName();
    }


}
