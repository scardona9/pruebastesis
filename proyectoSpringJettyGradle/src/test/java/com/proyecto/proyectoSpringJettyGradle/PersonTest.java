package com.proyecto.proyectoSpringJettyGradle;

import org.junit.Test;

import com.proyecto.proyectoSpringJettyGradle.model.Person;

import static org.junit.Assert.*;

public class PersonTest {
	
    @Test
    public void canConstructAPersonWithAName() {
        Person person = new Person("Larry", "James");
        assertEquals("Larry", person.getName());
    }
}
