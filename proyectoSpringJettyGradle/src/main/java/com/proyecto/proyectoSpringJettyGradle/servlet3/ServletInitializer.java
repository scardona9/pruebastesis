package com.proyecto.proyectoSpringJettyGradle.servlet3;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import com.proyecto.proyectoSpringJettyGradle.spring.config.SpringRootConfig;
import com.proyecto.proyectoSpringJettyGradle.spring.config.SpringWebConfig;

public class ServletInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {
	
	private static final String DEFAULT_PROFILE = "dev";

	@Override
	protected Class<?>[] getServletConfigClasses() {
		return new Class[] { SpringWebConfig.class };
	}

	@Override
	protected String[] getServletMappings() {
		return new String[] { "/" };
	}

	@Override
	protected Class<?>[] getRootConfigClasses() {
		return new Class[] { SpringRootConfig.class };
	}
	
	@Override
	protected WebApplicationContext createServletApplicationContext() {
		// TODO Auto-generated method stub
		AnnotationConfigWebApplicationContext context = (AnnotationConfigWebApplicationContext) super.createServletApplicationContext();
		context.getEnvironment().setDefaultProfiles(DEFAULT_PROFILE);
		return context;
	}
}
