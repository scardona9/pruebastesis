package com.proyecto.proyectoSpringJettyGradle.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.proyecto.proyectoSpringJettyGradle.model.Person;

@Controller
public class IndexController {

    @Value("${example.message}")
    private String message;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    @ResponseBody
    public String showIndex() {
        return message;
    }
    
    @RequestMapping(value = "/person", method = RequestMethod.GET)
    @ResponseBody
    public Person showPerson() {
    	Person p = new Person("pepe","sanchez");
        return p;
    }
    
    @RequestMapping(value = "/person/fullName", method = RequestMethod.POST)
    @ResponseBody
    public String showPersonFullName(@RequestBody Person p) {
        return p.getName() + " " + p.getLastName();
    }
}
