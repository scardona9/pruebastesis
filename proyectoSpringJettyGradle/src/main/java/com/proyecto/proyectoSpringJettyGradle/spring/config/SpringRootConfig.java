package com.proyecto.proyectoSpringJettyGradle.spring.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({"com.proyecto.proyectoSpringJettyGradle.service"})
public class SpringRootConfig {
}
