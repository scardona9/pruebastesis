package com.prueba.proyectoSpringJerseyJettyHibernateGradle.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.prueba.proyectoSpringJerseyJettyHibernateGradle.model.Person;

@Repository
public class PersonDaoImpl implements PersonDao {

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private SessionFactory sessionFactory;

	protected Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	public List<Person> getPersons() {
		Criteria crit = getCurrentSession().createCriteria(Person.class);
		List<Person> list = crit.list();
		return list;
	}

	public Person getPersonById(Long id) {
		Criteria crit = getCurrentSession().createCriteria(Person.class);
		crit.add( Restrictions.eq("id", id ) );
		crit.setCacheable(true);
		return (Person)crit.uniqueResult();
	}

	public Long deletePersonById(Long id) {
		Criteria crit = getCurrentSession().createCriteria(Person.class);
		crit.add( Restrictions.eq("id", id ) );
		crit.setCacheable(true);
		Person person = (Person) crit.uniqueResult();
		getCurrentSession().delete(person);
		return null;
	}

	public Long createPerson(Person person) {
		getCurrentSession().save(person);
		return null;
	}

	public int updatePerson(Person person) {
		getCurrentSession().update(person);
		return 0;
	}

	public void deletePersons() {
		// TODO Auto-generated method stub
	}

}
