package com.prueba.proyectoSpringJerseyJettyHibernateGradle.dao;

import java.util.List;

import com.prueba.proyectoSpringJerseyJettyHibernateGradle.model.Person;

public interface PersonDao {
	
	public List<Person> getPersons();
	 
	public Person getPersonById(Long id);
 
	public Long deletePersonById(Long id);
 
	public Long createPerson(Person person);
 
	public int updatePerson(Person person);
 
	/** removes all Persons */
	public void deletePersons();	

}
