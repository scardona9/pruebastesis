package com.prueba.proyectoSpringJerseyJettyHibernateGradle.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {
	    "com.prueba.proyectoSpringJerseyJettyHibernateGradle.service",
	    "com.prueba.proyectoSpringJerseyJettyHibernateGradle.common",
	    "com.prueba.proyectoSpringJerseyJettyHibernateGradle.resource",
	    "com.prueba.proyectoSpringJerseyJettyHibernateGradle.model",
	    "com.prueba.proyectoSpringJerseyJettyHibernateGradle.dao"
	})
public class SpringAppConfiguration {
}
