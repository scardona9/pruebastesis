package com.prueba.proyectoSpringJerseyJettyHibernateGradle.config;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;

@ApplicationPath("/rest")
public class JerseyAppConfiguration extends ResourceConfig {
	
    public JerseyAppConfiguration() {
        register(MyObjectMapperProvider.class);
        packages("com.prueba.proyectoSpringJerseyJettyHibernateGradle");
        //register(LoggingFeature.class);
    }

}
