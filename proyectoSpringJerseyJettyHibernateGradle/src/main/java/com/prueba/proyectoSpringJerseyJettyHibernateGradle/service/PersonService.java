package com.prueba.proyectoSpringJerseyJettyHibernateGradle.service;

import java.util.List;

import com.prueba.proyectoSpringJerseyJettyHibernateGradle.model.Person;

public interface PersonService {
	
	public List<Person> getPersons();
	
	public Person getPersonById(Long id);
	 
	public Long deletePersonById(Long id);
 
	public Long createPerson(Person person);
 
	public int updatePerson(Person person);
 
	/** removes all Persons */
	public void deletePersons();	

}
