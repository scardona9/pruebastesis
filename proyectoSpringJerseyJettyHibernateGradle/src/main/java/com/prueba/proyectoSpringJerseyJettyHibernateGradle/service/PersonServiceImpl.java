package com.prueba.proyectoSpringJerseyJettyHibernateGradle.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.prueba.proyectoSpringJerseyJettyHibernateGradle.dao.PersonDao;
import com.prueba.proyectoSpringJerseyJettyHibernateGradle.model.Person;

@Service
@Transactional
public class PersonServiceImpl implements PersonService{
	
	@Autowired
	private PersonDao personDao;
	
	public List<Person> getPersons() {
		return personDao.getPersons();
	}

	public Person getPersonById(Long id) {
		return personDao.getPersonById(id);
	}

	public Long deletePersonById(Long id) {
		return personDao.deletePersonById(id);
	}

	public Long createPerson(Person person) {
		return personDao.createPerson(person);
	}

	public int updatePerson(Person person) {
		return personDao.updatePerson(person);
	}

	public void deletePersons() {
		// TODO Auto-generated method stub
		
	}

}
