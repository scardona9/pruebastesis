package com.prueba.proyectoSpringJerseyJettyHibernateGradle.resource;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.prueba.proyectoSpringJerseyJettyHibernateGradle.model.Person;
import com.prueba.proyectoSpringJerseyJettyHibernateGradle.service.PersonService;

@Component
@Path("/persons")
public class PersonResource {
	
	@Autowired
	private PersonService personService;
	
	@Value("${example.message}")
	private String message;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Person> getPersons() {
		List<Person> persons = personService.getPersons();
		return persons;
	}
	
	@GET
	@Path("{Id}")
	@Produces(MediaType.APPLICATION_JSON)
    public Response getPersonById(@PathParam("Id") Long id) {
		Person person = personService.getPersonById(id);
        return Response.status(201)
				.entity(person).build();
    }
	
	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.TEXT_HTML })
	public Response createPerson(Person person) {
		personService.createPerson(person);
		return Response.status(201)
				.entity("A new person/resource has been created").build();
	}
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	public Response updatePerson(Person person) {
		personService.updatePerson(person);
		return Response.status(201)
				.entity("The person/resource has been updated").build();
	}
	
	@DELETE
	@Path("{Id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deletePersonById(@PathParam("Id") Long id) {
		personService.deletePersonById(id);
		return Response.status(201)
				.entity("The person/resource has been deleted").build();
	}
}
