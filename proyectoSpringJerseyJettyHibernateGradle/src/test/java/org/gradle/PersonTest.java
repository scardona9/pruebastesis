package org.gradle;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.prueba.proyectoSpringJerseyJettyHibernateGradle.model.Person;

public class PersonTest {
    @Test
    public void canConstructAPersonWithAName() {
        Person person = new Person("Larry", "Johnson");
        assertEquals("Larry", person.getName());
    }
}
