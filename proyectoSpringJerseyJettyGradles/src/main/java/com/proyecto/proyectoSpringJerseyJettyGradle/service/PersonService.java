package com.proyecto.proyectoSpringJerseyJettyGradle.service;

import org.springframework.stereotype.Service;

import com.proyecto.proyectoSpringJerseyJettyGradle.model.Person;

@Service
public class PersonService implements IPersonService{
	
	public String getPersonName(){
		Person person = new Person("pepe", "sanchez");
		return person.getName();
	}
	
	public Person getPerson(){
		return new Person("cacho", "rodriguez");
	}

}
