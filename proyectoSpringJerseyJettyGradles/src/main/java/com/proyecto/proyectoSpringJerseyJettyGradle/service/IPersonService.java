package com.proyecto.proyectoSpringJerseyJettyGradle.service;

import com.proyecto.proyectoSpringJerseyJettyGradle.model.Person;

public interface IPersonService {
	
	String getPersonName();
	
	Person getPerson();

}
