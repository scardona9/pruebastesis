package com.proyecto.proyectoSpringJerseyJettyGradle.resource;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.proyecto.proyectoSpringJerseyJettyGradle.model.Person;
import com.proyecto.proyectoSpringJerseyJettyGradle.service.IPersonService;

@Component
@Path("/person")
public class PersonResource {
	
	@Autowired
	private IPersonService personService;
	
	@Value("${example.message}")
	private String message;
	
	@GET
	@Path("/name")
    public Response getName() {
        return Response.ok(personService.getPersonName() + "-" + message).build();
    }
	
	@GET
	@Path("/body")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getPerson() {
		Person person = personService.getPerson();
		return Response.ok(person).build();
	}

}
