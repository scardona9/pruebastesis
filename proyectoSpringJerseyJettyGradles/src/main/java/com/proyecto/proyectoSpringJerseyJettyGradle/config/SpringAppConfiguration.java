package com.proyecto.proyectoSpringJerseyJettyGradle.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {
	    "com.proyecto.proyectoSpringJerseyJettyGradle.service",
	    "com.proyecto.proyectoSpringJerseyJettyGradle.common",
	    "com.proyecto.proyectoSpringJerseyJettyGradle.resource",
	    "com.proyecto.proyectoSpringJerseyJettyGradle.model"
	})
public class SpringAppConfiguration {
}
