package com.proyecto.proyectoSpringJerseyJettyGradle.config;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;

@ApplicationPath("/rest")
public class JerseyAppConfiguration extends ResourceConfig {
	
    public JerseyAppConfiguration() {
        //register(RequestContextFilter.class);
        packages("com.proyecto.proyectoSpringJerseyJettyGradle");
        //register(LoggingFeature.class);
    }

}
