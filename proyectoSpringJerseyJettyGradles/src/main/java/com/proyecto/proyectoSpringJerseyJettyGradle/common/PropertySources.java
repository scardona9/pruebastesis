package com.proyecto.proyectoSpringJerseyJettyGradle.common;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

@Configuration
public class PropertySources {

    @Profile("dev")
	@PropertySource("classpath:example-dev.properties")
    public static class DevConfig {
        @Bean
	    public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
	        PropertySourcesPlaceholderConfigurer pspc = new PropertySourcesPlaceholderConfigurer();
	        return pspc;
	    }
    }

    @Profile("test")
    @PropertySource("classpath:example-test.properties")
    @Configuration
    public static class TestConfig {
        @Bean
        public static PropertySourcesPlaceholderConfigurer propertyConfigInTest() {
            PropertySourcesPlaceholderConfigurer pspc = new PropertySourcesPlaceholderConfigurer();
            return pspc;
        }
    }

    @Profile("prod")
    @PropertySource("classpath:example-prod.properties")
    @Configuration
    public static class ProdConfig {
        @Bean
        public static PropertySourcesPlaceholderConfigurer propertyConfigInProd() {
            PropertySourcesPlaceholderConfigurer pspc = new PropertySourcesPlaceholderConfigurer();
            return pspc;
        }
    }
}